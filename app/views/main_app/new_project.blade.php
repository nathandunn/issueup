@extends('layouts.main')

@section('styles')
	<style type="text/css">
		#main{
			padding-top: 60px;
		}
		form{
  			max-width: 330px;
  			padding: 15px;
  			margin: 0 auto;
		}
		textarea{
			resize: none;
		}
	</style>
@stop

@section('content')
	<div class="container" id="main">
		{{ Form::open(['route' => 'project.store']) }}
			<h2>New Project</h2>

			<div class="form-group">
				<div class="input-group">
					<span class="input-group-addon"><i class="fa fa-pencil"></i></span>
					{{ Form::text('name', '', [
						'class' 		=> 'form-control',
						'placeholder' 	=> 'Project Name'
						]) }}
				</div>
			</div>

			<div class="form-group">
				<div class="input-group">
					<span class="input-group-addon"><i class="fa fa-link"></i></span>
					{{ Form::text('url', '', [
						'class' 		=> 'form-control',
						'placeholder' 	=> 'Project URL',
						]) }}
				</div>
			</div>

			<div class="form-group">
				<div class="input-group">
					<span class="input-group-addon"><i class="fa fa-github"></i></span>
					{{ Form::text('github', NULL, ['class' => 'form-control', 
						'placeholder' => 'GitHub Link (example/example)']) }}
				</div>
			</div>

			<div class="form-group">
				{{ Form::textarea('desc', '', [
					'class'			=> 'form-control', 
					'placeholder' 	=> 'Description'
					]) 
				}}
			</div>

			<div class="form-group">
				{{ Form::submit('Add Project', ['class' => 'btn btn-primary btn-lg btn-block']) }}
			</div>

		{{ Form::close() }}
	</div>
@stop

@if ($errors->has())
	@section('scripts')
		<script type="text/javascript">
			new notif({
				type: "error",
				msg: "<b>Error:</b>&nbsp;<ul>@foreach ($errors->all() as $error)<li>{{ $error }}</li>@endforeach</ul>",
				position: "right",
				autohide: false,
				multiline: true
			});
		</script>
	@stop
@endif
