@extends('layouts.main')

@section('styles')
<style type="text/css">
	#main{
		max-width: 400px;
		padding-top: 80px;
	}
</style>
@stop


@section('content')

<div class="container" id="main">
	{{ Form::model($project, ['method' => 'put', 'route' => ['project.update', $project->id]]) }}


			<h2>Edit Project</h2>

			<div class="form-group">
				<div class="input-group">
					<span class="input-group-addon"><i class="fa fa-pencil"></i></span>
					{{ Form::text('name', NULL, [
						'class' 		=> 'form-control',
						'placeholder' 	=> 'Project Name'
						]) }}
				</div>
			</div>

			<div class="form-group">
				<div class="input-group">
					<span class="input-group-addon"><i class="fa fa-link"></i></span>
					{{ Form::text('url', NULL, [
						'class' 		=> 'form-control',
						'placeholder' 	=> 'Project URL',
						]) }}
				</div>
			</div>

			<div class="form-group">
				<div class="input-group">
					<span class="input-group-addon"><i class="fa fa-github"></i></span>
					{{ Form::text('github_url', NULL, ['class' => 'form-control', 
						'placeholder' => 'GitHub Link (example/example)']) }}
				</div>
			</div>

			<div class="form-group">
				{{ Form::textarea('desc', NULL, [
					'class'			=> 'form-control', 
					'placeholder' 	=> 'Description'
					]) 
				}}
			</div>

			<div class="form-group">
				{{ Form::submit('Save Changes', ['class' => 'btn btn-primary btn-lg btn-block']) }}
			</div>

	{{ Form::close() }}

	{{ Form::open(['method' => 'delete', 'route' => ['project.destroy', $project->id]]) }}

		{{ Form::submit('Delete', ['class' => 'btn btn-danger btn-lg btn-block']) }}

	{{ Form::close() }}

</div>

@stop