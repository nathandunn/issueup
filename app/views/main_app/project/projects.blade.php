@extends('layouts.main')

@section('styles')
	<style type="text/css">
		#main{
			max-width: 400px;
			padding-top: 80px;
		}
		.item{
			border: 3px solid #34a0c7;
			border-radius: 10px;
			margin: 20px 0px;
		}
		.item h3{
			margin: 0px;
			margin: 10px 20px;
		}
		.item p{
			margin: 0px;
			margin: 10px 20px;
		}
		.edit{
			margin-right: 10px;
			margin-top: 5px;
		}
	</style>
@stop


@section('content')
	
	<div class="container" id="main">
		@foreach ($projects as $project)
			<div class="item">
				<div class="row">
					<div class="col-md-8">
						<h3><a href="/project/{{{ $project->id }}}">{{{$project->name}}}</a></h3>
					</div>
					<div class="col-md-4">
						<a href="/project/{{{ $project->id }}}/edit/" class="btn btn-default edit">
							Edit Project
						</a>
					</div>
				</div>
				<p>{{{ $project->desc }}}</p>
				@if($project->github_url != NULL)
					<p>
						<i class="fa fa-github"></i>&nbsp;
						<a href="{{{ $project->github_url }}}">
							{{{ $project->github_url }}}
						</a>
					</p>
				@endif

				@if($project->url != NULL)
					<p>
						<i class="fa fa-link"></i>&nbsp;
						<a href="{{{ $project->url }}}">
							{{{ $project->url }}}
						</a>
					</p>
				@endif
			</div>
		@endforeach
		<div class="text-center">
			{{ $projects->links(); }}
		</div>
	</div>
@stop


@section('scripts')
	@if (Session::get('success'))
		@section('scripts')
			<script type="text/javascript">
				new notif({
					type: "success",
					msg: "Changes saved successfully",
					position: "right",
					autohide: false,
					multiline: true
				});
			</script>
		@stop
	@endif
@stop