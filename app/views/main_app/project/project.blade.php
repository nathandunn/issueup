@extends('layouts.main')

@section('styles')
<style type="text/css">
	#main{
		max-width: 450px;
		padding-top: 80px;
	}
	h2{
		text-align: center;
	}
	.item{
		border: 3px solid #34a0c7;
		border-radius: 10px;
		margin: 20px 0px;
	}
	.item h3{
		margin: 0px;
		margin: 10px 20px;
	}
	.item p{
		margin: 0px;
		margin: 10px 20px;
	}
	#issues{
		padding: 10px 0px; 
		text-align: center;
	}
</style>
@stop

@section('content')
	<div class="container" id="main">	

		<h2>Issues related to {{{$project->name}}}</h2>

		<div class="text-center">
			<a class="btn btn-success" href="{{ route('project.issue.create', $project->id) }}">Add New Project</a>
		</div>

		@if($issue_count > 0)
			@foreach($issues as $issue)
				<div class="item">
					<div class="row">
						<div class="col-md-8"><h3>{{{$issue->title}}}</h3></div>
						<div class="col-md-4"><a class="btn btn-default" href="{{ route('project.issue.edit', [$project->id, $issue->id]) }}">Edit Project</a></div>
					</div>
					<p>{{{ $issue->desc }}}</p>
				</div>
			@endforeach
		@else
			<p id="issues">No issues to show.</p>
		@endif
		<div class="text-center">
			{{ $issues->links(); }}
		</div>
	</div>

@stop

