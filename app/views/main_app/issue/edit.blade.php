@extends('layouts.main')

@section('styles')
	<style type="text/css">
		#main{
			max-width: 400px;
			padding-top: 80px;
		}
	</style>
@stop

@section('content')
	<div class="container" id="main">
		{{ Form::model($issue, ['route' => ['project.issue.update', $project->id]]) }}
			<h2>Edit Issue</h2>

			<div class="form-group">
				{{ Form::text('title', NULL, [
					'placeholder' 	=> 'Issue Title',
					'class'			=> 'form-control'
					])
				}}
			</div>

			<div class="form-group">			
				{{ Form::textarea('desc', NULL, [
					'placeholder' 	=> 'Issue Description',
					'class'			=> 'form-control'
					])
				}}
			</div>

			<div class="form-group">			
				{{ Form::submit('Add Issue', [
					'class'	=> 'btn btn-lg btn-primary btn-block'
					])
				}}
			</div>

		{{ Form::close() }}
		
		{{ Form::open(['method' => 'delete', 'route' => ['project.issue.destroy', $project->id, $issue->id]]) }}

			{{ Form::submit('Delete', ['class' => 'btn btn-danger btn-lg btn-block']) }}

		{{ Form::close() }}

	</div>
@stop