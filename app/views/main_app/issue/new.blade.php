@extends('layouts.main')

@section('styles')
	<style type="text/css">
		#main{
			max-width: 400px;
			padding-top: 80px;
		}
	</style>
@stop

@section('content')
	<div class="container" id="main">
		{{ Form::open(['route' => ['project.issue.store', $project->id]]) }}
			<h2>New Issue</h2>

			<div class="form-group">
				{{ Form::text('title', NULL, [
					'placeholder' 	=> 'Issue Title',
					'class'			=> 'form-control'
					])
				}}
			</div>

			<div class="form-group">			
				{{ Form::textarea('desc', NULL, [
					'placeholder' 	=> 'Issue Description',
					'class'			=> 'form-control'
					])
				}}
			</div>

			<div class="form-group">			
				{{ Form::submit('Add Issue', [
					'class'	=> 'btn btn-lg btn-primary btn-block'
					])
				}}
			</div>

		{{ Form::close() }}
		
	</div>
@stop