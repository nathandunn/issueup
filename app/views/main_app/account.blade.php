@extends('layouts.main')

@section('styles')
	<style type="text/css">
		#main{
			padding-top: 60px;
		}
		form{
  			max-width: 330px;
  			padding: 15px;
  			margin: 0 auto;
		}
	</style>
@stop

@section('content')
	<div class="container" id="main">

		{{ Form::model($user) }}

			<h2>Account</h2>

			<div class="form-group">
				<div class="input-group">
					<span class="input-group-addon"><i class="fa fa-user"></i></span>
					{{ Form::text('username', NULL, ['class' => 'form-control']) }}
				</div>
			</div>
			
			<div class="form-group">
				<div class="input-group">
					<span class="input-group-addon"><i class="fa fa-envelope"></i></span>
					{{ Form::email('email', NULL, ['class' => 'form-control']) }}
				</div>
			</div>

			<div class="form-group">
				<div class="input-group">
					<span class="input-group-addon"><i class="fa fa-github"></i></span>
					{{ Form::text('github', NULL, ['class' => 'form-control', 'placeholder' => 'GitHub Username']) }}
				</div>
			</div>

			<div class="form-group">
				{{ Form::submit('Save', ['class' => 'btn btn-primary btn-lg btn-block']) }}
			</div>

		{{ Form::close() }}

	</div>
@stop

@if ($errors->has())
	@section('scripts')
		<script type="text/javascript">
			new notif({
				type: "error",
				msg: "<b>Error:</b>&nbsp;<ul>@foreach ($errors->all() as $error)<li>{{ $error }}</li>@endforeach</ul>",
				position: "right",
				autohide: false,
				multiline: true
			});
		</script>
	@stop
@endif

@if (Session::get('success'))
	@section('scripts')
		<script type="text/javascript">
			new notif({
				type: "success",
				msg: "Changes saved successfully",
				position: "right",
				autohide: false,
				multiline: true
			});
		</script>
	@stop
@endif


