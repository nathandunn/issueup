@extends('layouts.main')

@section('styles')
<style type="text/css">
	#main{
		max-width: 400px;
		padding-top: 80px;
	}
	.item{
		border: 3px solid #34a0c7;
		border-radius: 10px;
		margin: 20px 0px;
	}
	.item h3{
		margin: 0px;
		margin: 10px 20px;
	}
	.item p{
		margin: 0px;
		margin: 10px 20px;
	}

</style>
@stop

@section('content')
	<div class="container" id="main">
		<h1>All issues related to *project name*</h1>
	</div>

@stop

