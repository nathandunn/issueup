@extends('layouts.main')

@section('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/auth.css') }}">
@stop

@section('content')

<div class="container">

	{{ Form::open(['class' => 'auth-form register']) }}

		<h1 class="auth-form-heading">Register</h1>
	    
		{{-- Form::text('first_name', '', ['class' => 'form-control', 'placeholder' => 'First Name']) --}}
		{{-- Form::text('last_name', '', ['class' => 'form-control', 'placeholder' => 'Last Name']) --}}
		
		{{ Form::text('username', '', ['class' => 'form-control', 'placeholder' => 'Username']) }}

	    {{ Form::email('email', '', ['class' => 'form-control', 'placeholder' => 'Email Address']) }}
	    
	    {{ Form::password('password', ['class' => 'form-control', 'placeholder' => 'Password']) }}
	    {{ Form::password('password_confirmation', ['class' => 'form-control', 'placeholder' => 'Confirm Password']) }}

	    {{ Form::submit('Register', ['class' => 'btn btn-lg btn-login btn-block']) }}

	    <p>By clicking Register, you are agreeing to our <a href="">terms and conditions</a> and <a href="">privacy policy</a>.</p>

	{{ Form::close() }}

</div>

@stop