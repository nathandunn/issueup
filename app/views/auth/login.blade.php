@extends('layouts.main')

@section('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/auth.css') }}">
@stop

@section('content')

	<div class="container">

		{{ Form::open(['class' => 'auth-form']) }}

			<h2 class="auth-form-heading">Log In</h2>
		    
		    {{ Form::text('email', '', ['class' => 'form-control email', 'placeholder' => 'Email Address or Username']) }}
		    
		    {{ Form::password('password', ['class' => 'form-control password', 'placeholder' => 'Password']) }}
		    
			<div class="checkbox">
				<label>
					<input type="checkbox" value="1"> Remember me
				</label>
			</div>

		    {{ Form::submit('Log In', ['class' => 'btn btn-lg btn-login btn-block']) }}
		    <p><a href="">Forgot your password?</a></p>
		{{ Form::close() }}

	</div>

@stop