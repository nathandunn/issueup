@extends('layouts.main')

<div class="container">

	{{ Form::open(['class' => 'auth-form register']) }}

		<h1 class="auth-form-heading">Student Shared</h1>

	    {{ Form::email('email', '', ['class' => 'form-control', 'placeholder' => 'Email Address']) }}

	    {{ Form::submit('Submit', ['class' => 'btn btn-lg btn-login btn-block']) }}

	{{ Form::close() }}

</div>