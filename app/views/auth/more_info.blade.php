@extends('layouts.main')

<div class="container">

	{{ Form::open(['class' => 'auth-form register']) }}

		<h1 class="auth-form-heading">Student Shared</h1>
	
		<div class="info-box">
		    
			<h3>First time set-up</h3>
			<p>
				Before you can use Student Shared, please provide us with some additional information.
			</p>
		</div>
		{{ Form::text('first_name', '', ['class' => 'form-control', 'placeholder' => 'First Name']) }}
		{{ Form::text('last_name', '', ['class' => 'form-control', 'placeholder' => 'Last Name']) }}
		
		{{ Form::text('establishment', '', ['class' => 'form-control', 'placeholder' => 'Institution Name']) }}
		<!-- AJAX-y stuff -->

		<div class="results">
			<ul>
			</ul>
		</div>


	    {{ Form::submit('Continue', ['class' => 'btn btn-lg btn-login btn-block']) }}

	{{ Form::close() }}

</div>