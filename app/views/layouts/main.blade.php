<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>{{ 'IssueUp' }}</title>

		<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/bootstrap.min.css') }}">
		<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/font-awesome.min.css') }}">
		<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/notifIt.css') }}">
		<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/main.css') }}">

		{{-- Styles can be added in this section --}}
		@yield('styles')
	
	</head>
	<body>
		@include('partials.nav')

		@yield('content')

		<script type="text/javascript" src="{{ asset('assets/js/jquery-2.1.1.min.js') }}"></script>
		<script type="text/javascript" src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
		<script type="text/javascript" src="{{ asset('assets/js/notifIt.js') }}"></script>
		@if (Session::get('error'))
			<?php $errors = Session::get('error'); ?>

			<script type="text/javascript">
				new notif({
					type: "error",
					msg: "<b>Error:</b>@if (is_array($errors) == true)<ul>@foreach ($errors as $error)<li>{{{ $error }}}</li>@endforeach</ul>@else {{{ $errors }}} @endif",
					position: "right",
					autohide: false,
					multiline: true
				});
			</script>
		@endif
	
		{{-- Scripts can be added in this section --}}
		@yield('scripts')
	
	</body> 
</html>