<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
	<div class="container">
				<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="#">IssueUp</a>
		</div>

				<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav">
				@if(Auth::check())
					@foreach($menus->menu('main-nav-in')->getItems() as $item)
						<li @if($item->isActive()) class="active" @endif>
							<a href="{{ $item['link'] }}">{{ $item['title'] }}</a>
						</li>
					@endforeach
				@else
					@foreach($menus->menu('main-nav')->getItems() as $item)
						<li @if($item->isActive()) class="active" @endif>
							<a href="{{ $item['link'] }}">{{ $item['title'] }}</a>
						</li>
					@endforeach
				@endif
			</ul>
			<ul class="nav navbar-nav navbar-right">
				@if(Auth::check())
					@foreach($menus->menu('right-auth-nav-in')->getItems() as $item)
						<li @if($item->isActive()) class="active" @endif>
							<a href="{{ $item['link'] }}">{{ $item['title'] }}</a>
						</li>
					@endforeach
					@foreach($menus->menu('right-auth-nav-in')->item('newProject') as $item)
						<li @if($item->isActive()) class="active" @endif>
							<a href="{{ $item['link'] }}">{{ $item['title'] }}</a>
						</li>
					@endforeach
				@else
					@foreach($menus->menu('right-auth-nav')->getItems() as $item)
						<li @if($item->isActive()) class="active" @endif>
							<a href="{{ $item['link'] }}">{{ $item['title'] }}</a>
						</li>
					@endforeach
				@endif
			</ul>
		</div><!-- /.navbar-collapse -->
	</div><!-- /.container-fluid -->
</nav>
