<?php

class MenuComposer {
	public function compose($view) {
		$menus = new Tlr\Menu\MenuRepository;

		$mainMenu 			= $menus->menu('main-nav');
		$authMenu 			= $menus->menu('right-auth-nav');
		$LoggedInMenuLeft 	= $menus->menu('main-nav-in');
		$LoggedInMenuRight 	= $menus->menu('right-auth-nav-in');
		

//	START main menu
		//$mainMenu->item( 'home', '<i class="fa fa-home"></i>&nbsp;Home', '/' );
//	END main menu

//	START auth menu (when not authenticated)
		$register 	= $authMenu->item( 'register', '<i class="fa fa-pencil"></i>&nbsp;Register', '/register' );
		$login 		= $authMenu->item( 'login', '<i class="fa fa-sign-in"></i>&nbsp;Login', '/login' );
// END auth menu

//	START left menu (when authenticated)
		$account 	= $LoggedInMenuRight->item( 'account', '<i class="fa fa-user"></i>&nbsp;Account', '/account' );
		$logout 	= $LoggedInMenuRight->item( 'logout', '<i class="fa fa-sign-out"></i>&nbsp;Logout', '/logout' );
// END auth menu


//	START right auth menu (when authenticated)
		$projects 	= $LoggedInMenuLeft->item( 'projects', '<i class="fa fa-bolt"></i>&nbsp;Projects', '/projects' );
			$LoggedInMenuLeft->item( 'newProject', '<i class="fa fa-plus"></i>&nbsp;New Project', '/project/create' );
		//$LoggedInMenuLeft->item( 'feed', '<i class="fa fa-home"></i>&nbsp;Feed', '/' );
// END auth menu

		$mainMenu->activate( $_SERVER["REQUEST_URI"] );
		$authMenu->activate( $_SERVER["REQUEST_URI"] );
		$LoggedInMenuLeft->activate( $_SERVER["REQUEST_URI"] );
		$LoggedInMenuRight->activate( $_SERVER["REQUEST_URI"] );
		$view->with('menus', $menus);
	}
}