<?php

class ProjectsController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$userId 	= Auth::id();
		$projects 	= Project::where('user_id', '=', $userId)->paginate(10);

		return View::make('main_app.project.projects')->withProjects($projects);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('main_app.new_project');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$userId = Auth::id();

		$github_project = Input::get('github');
		$github_address = 'http://github.com/' . $github_project;

		Input::merge(['github' => $github_address]);

		//dd(Input::all());

		$rules = [
			'name' => 'required',
			'url' => 'url',
			'github' => 'url|required'
		];

		$validator = Validator::make(Input::all(), $rules);

		if($validator->fails()){
			$messages = $validator->messages();
			return View::make('main_app.new_project')->withErrors($messages)->withInput(Input::all());
		}

		$project 				= new Project;
		$project->user_id 		= $userId;
		$project->name 			= Input::get('name');
		$project->desc 			= Input::get('desc');
		$project->url 			= Input::get('url');
		$project->github_url 	= Input::get('github');
		$project->save();


		Session::flash('success', 'Project added successfully');
		return Redirect::to('/projects');
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$issues 	= Issue::where('project_id', '=', $id)->paginate(10);
		$issueCount = Issue::where('project_id', '=', $id)->count();
		$project 	= Project::find($id);
		return View::make('main_app.project.project')
			->withProject($project)
			->withIssues($issues)
			->withIssue_count($issueCount);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$project = Project::find($id);

		return View::make('main_app.project.edit')->withProject($project);

	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$rules = [
			'name' => 'required',
			'url' => 'url',
			'github' => 'url|required'
		];

		$validator = Validator::make($rules, Input::all());

		if($validator->fails()){
			$messages = $validator->messages();
			return View::make('main_app.edit')->withErrors($messages)->withInput(Input::all());
		}

		$project = Project::find($id);

		$project->name 			= Input::get('name');
		$project->url 			= Input::get('url');
		$project->github_url	= Input::get('name');
		$project->desc			= Input::get('desc');
		$project->save();

		Session::flash('success', 'Changes saved successfully');
		return Redirect::to('/projects');
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$project = Project::find($id);

		$project->delete();

		Session::flash('success', 'Project deleted successfully');
		return Redirect::to('/projects');

	}


}
