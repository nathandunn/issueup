<?php

class ProjectIssueController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create($projectId)
	{
		$project = Project::find($projectId);
		return View::make('main_app.issue.new')->withProject($project);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store($projectId)
	{
		$userId = Auth::id();

		$project = Project::find($projectId);
		
		$rules = [
			'title' => 'required',
			'desc' => 'required'
		];

		$validator = Validator::make(Input::all(), $rules);

		if ($validator->fails()) {
			$messages = $validator->messages();
			return View::make('main_app.issue.new')
				->withErrors($messages)
				->withInput(Input::all())
				->withProject($project);
		}

		$issue = new Issue;
		$issue->user_id 	= $userId;		
		$issue->project_id 	= $projectId;		
		$issue->title 		= Input::get('title');
		$issue->desc 		= Input::get('desc');
		$issue->save();

		return Redirect::to('/project/' . $projectId);

	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($issueId)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($projectId, $issueId)
	{
		$issue 		= Issue::find($issueId);
		$project 	= Project::find($projectId);

		return View::make('main_app.issue.edit')->withProject($project)->withIssue($issueId);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($projectId, $issueId)
	{
		$project = Project::find($projectId);

		$rules = [
			'title' => 'required',
			'desc' => 'required'
		];

		$validator = Validator::make(Input::all(), $rules);

		if ($validator->fails()) {
			$messages = $validator->messages();
			return View::make('main_app.issue.new')
				->withErrors($messages)
				->withInput(Input::all())
				->withProject($project);
		}

		$issue = Issue::find($issueId);
		$issue->user_id 	= $userId;		
		$issue->project_id 	= $projectId;		
		$issue->title 		= Input::get('title');
		$issue->desc 		= Input::get('desc');
		$issue->save();



	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($issueId)
	{
		//
	}


}
