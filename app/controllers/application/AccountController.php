<?php

class AccountController extends BaseController{

	public function index(){
		$userId = Auth::id();
		$user = User::find($userId);

		return View::make('main_app.account')->withUser($user);
	}

	public function post(){

		$rules = [
			'username' 	=> 'required',
			'email' 	=> 'required|email'
		];

		$validator = Validator::make(Input::all(), $rules);

		$userId = Auth::id();
		$user = User::find($userId);

		if ($validator->fails()) {
			$messages = $validator->messages();
			return View::make('main_app.account')->withErrors($messages)->withUser($user);
			//return View::make('main_app.account')->withErrors($validator)->withInput();
		}
		
		$user->username = Input::get('username');
		$user->email 	= Input::get('email');
		$user->github 	= Input::get('github');
		$user->save();


		$successMessage = 'Changes saved successfully';
		
		$success = Session::flash('success', $successMessage);
		return Redirect::to('/projects');

	}

}