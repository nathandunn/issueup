<?php 

class HomePageController extends BaseController{

	public function index(){

		if (Auth::check()) {
			return Redirect::to('/projects');
		}

		return Redirect::to('/login');
	}
}