<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatedTimestampsFields extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('projects', function($table){
			$table->timestamps();
		});

		Schema::table('votes', function($table){
			$table->timestamps();
		});

		Schema::table('issues', function($table){
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('projects', function($table){
			$table->dropTimestamps();
		});

		Schema::table('votes', function($table){
			$table->dropTimestamps();
		});

		Schema::table('issues', function($table){
			$table->dropTimestamps();
		});
	}

}
