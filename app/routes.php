<?php

Route::get('/', 'HomePageController@index');

// Confide routes
Route::get('register', 					['uses' => 'UsersController@create', 'name' => 'register']);
Route::post('register', 				'UsersController@store');

Route::get('login', 					'UsersController@login');
Route::post('login', 					'UsersController@doLogin');

Route::get('users/confirm/{code}', 		'UsersController@confirm');
Route::get('forgot_password', 			'UsersController@forgotPassword');
Route::post('forgot_password', 			'UsersController@doForgotPassword');
Route::get('reset_password/{token}', 	'UsersController@resetPassword');
Route::post('reset_password', 			'UsersController@doResetPassword');

// Main Application
Route::group(['before' => 'auth'], function(){
	Route::get('logout', 		'UsersController@logout');

	Route::get('account', 		'AccountController@index');
	Route::post('account', 		'AccountController@post');
	Route::resource('project', 	'ProjectsController');
	Route::get('projects', 		'ProjectsController@index');

	Route::resource('project.issue', 	'ProjectIssueController');
});

Route::when('*', 'csrf', array('post', 'put', 'delete'));

// Composers 
View::composer('partials.nav', 'MenuComposer');